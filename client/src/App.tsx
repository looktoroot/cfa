import React from 'react'
import {Provider} from 'react-redux'
import {BrowserRouter as Router} from 'react-router-dom'
import store from './store'
import AppRouter from './shared/AppRouter'
import Layout from './shared/Layout'

const App: React.FC = () => (
    <Provider store={store}>
        <Router>
            <Layout>
                <AppRouter />
            </Layout>
        </Router>
    </Provider>
)

export default App
