
export default class Roles {
    static GUEST = 'guest';
    static USER = 'user';
    static ADMIN = 'admin'

    static getKeys() {
        return [
            this.GUEST,
            this.USER,
            this.ADMIN,
        ];
    }

    static getLabels() {
        return {
            [this.GUEST]: 'Гость',
            [this.USER]: 'Пользователь',
            [this.ADMIN]: 'Админ',
        };
    };

    static getAuth() {
        return [
            this.USER,
            this.ADMIN,
        ];
    }
}
