import {useState, useEffect} from 'react'
import {AxiosResponse} from 'axios'

type Action<T> = () => Promise<AxiosResponse<T>>

const useApi = <T extends any>(action: Action<T>, callback?: (data: T) => void): [T | null, boolean, string | null] => {

    const [data, setData] = useState<T | null>(null)
    const [isLoading, setIsLoading] = useState<boolean>(true)
    const [error, setError] = useState<string | null>(null)

    useEffect(() => {
        action()
            .then(({data}) => {
                setError(null)
                setData(data)
                callback && callback(data)
            })
            .catch((err) => {
                setError('Что то пошло не так')
            })
            .finally(() => {
                setIsLoading(false)
            })
    }, [action, callback])

    return [data, isLoading, error]
}

export default useApi

