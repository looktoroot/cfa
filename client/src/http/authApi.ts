import http from '../http'
import jwtDecode from 'jwt-decode'
import {IUser} from '../types/IUser'

const TOKEN_KEY = 'token'

export interface IRegistrationValues {
    email: string,
    password: string
}

export interface ILoginValues {
    email: string,
    password: string
}

export const registration = async (values: IRegistrationValues): Promise<Pick<IUser, 'id'>> => {
    const {data} = await http.post('/api/v1/users/registration', values)
    return data
}

export const login = async (values: ILoginValues): Promise<IUser> => {
    const {data} = await http.post('/api/v1/users/login', values)
    localStorage.setItem(TOKEN_KEY, data.token)
    return jwtDecode(data.token)
}

export const logout = () => {
    localStorage.removeItem(TOKEN_KEY)
}

export const userInitRequest = () => http.get('api/v1/users/init')

