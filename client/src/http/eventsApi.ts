import http from '../http'
import {IAnyObject} from '../types/IAnyObject'
import {IEvent} from '../types/IEvent'

export type IEventValues = Omit<IEvent, 'id'>

export const createEvent = async (values: IEventValues): Promise<IEvent> => {
    const {data} = await http.post('/api/v1/events', values)
    return data
}

export const fetchAllEventsRequest = (params?: IAnyObject) => http.get('api/v1/events', {params})
export const fetchOneEventsRequest = (id: string, params?: IAnyObject) => http.get(`api/v1/events/${id}`, {params})


