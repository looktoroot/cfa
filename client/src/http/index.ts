import axios, {AxiosRequestConfig} from "axios";

const http = axios.create({
    baseURL: process.env.REACT_APP_API_URL
})

const authInterceptor = (config: AxiosRequestConfig): AxiosRequestConfig => {
    const token = localStorage.getItem('token')

    if (token) {
        config.headers.authorization = `Bearer ${token}`
    }

    return config
}

http.interceptors.request.use(authInterceptor)

export default http
