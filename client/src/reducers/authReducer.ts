import {createSlice, PayloadAction} from '@reduxjs/toolkit'
import {RootState} from '../store'
import {IUser} from '../types/IUser'

interface IAuthState {
    user: IUser | null
    isAuth: boolean
}

const initialState: IAuthState = {
    user: null,
    isAuth: false,
}


export const authReducer = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        setUser:
            (state,
             action: PayloadAction<IUser | null>) => {
                state.user = action.payload
            },
        setIsAuth:
            (state,
             action: PayloadAction<boolean>) => {
                state.isAuth = action.payload
            },
    },
})

//selectors
export const selectUser = (state: RootState) => state.auth.user
export const selectIsAuth = (state: RootState) => state.auth.isAuth

//actions
export const {setUser, setIsAuth} = authReducer.actions

export default authReducer.reducer
