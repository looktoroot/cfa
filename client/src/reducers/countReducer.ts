import {createSlice, PayloadAction} from '@reduxjs/toolkit'
import {RootState} from '../store'

interface ICountState {
    value: number
}

const initialState: ICountState = {
    value: 0,
}


export const countReducer = createSlice({
    name: 'count',
    initialState,
    reducers: {
        increment: (state) => {
            state.value = state.value + 1
        },
        decrement: (state) => {
            state.value = state.value - 1
        },
        incrementByAmount: (state, action: PayloadAction<number>) => {
            state.value = state.value + action.payload
        },
    }
})

//selectors
export const selectCount = (state: RootState) => state.count.value

//actions
export const { increment, decrement, incrementByAmount } = countReducer.actions

export default countReducer.reducer
