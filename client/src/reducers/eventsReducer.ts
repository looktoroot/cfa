import {createSlice, PayloadAction} from '@reduxjs/toolkit'
import {RootState} from '../store'
import {IEvent} from '../types/IEvent'

interface IEventsState {
    items: IEvent[]
    page: number
    totalCount: number
    limit: number
}

const initialState: IEventsState = {
    items: [],
    page: 1,
    totalCount: 0,
    limit: 10
}

export const eventsReducer = createSlice({
    name: 'events',
    initialState,
    reducers: {
        setEvents:
            (state,
             action: PayloadAction<IEvent[] | []>) => {
                state.items = action.payload
            },
        setEventsPage:
            (state,
             action: PayloadAction<number>) => {
                state.page = action.payload
            },
        setEventsTotalCount:
            (state,
             action: PayloadAction<number>) => {
                state.totalCount = action.payload
            },
        setEventsLimit:
            (state,
             action: PayloadAction<number>) => {
                state.limit = action.payload
            },
    },
})

//selectors
export const selectEvents = (state: RootState) => state.events.items
export const selectEventsPage = (state: RootState) => state.events.page
export const selectEventsTotalCount = (state: RootState) => state.events.totalCount
export const selectEventsLimit = (state: RootState) => state.events.limit

//actions
export const {setEvents, setEventsPage, setEventsTotalCount, setEventsLimit} = eventsReducer.actions

export default eventsReducer.reducer
