import React, {useCallback} from 'react'
import {useParams, useHistory} from 'react-router-dom'
import {Button, Spin} from 'antd'
import {format} from 'date-fns'
import {useApi, useAppSelector} from '../../hooks'
import {fetchOneEventsRequest} from '../../http/eventsApi'
import {IEvent} from '../../types/IEvent'
import bemClasses from '../../utils/bemClasses'
import {selectIsAuth} from '../../reducers/authReducer'
import './EventPage.scss'

interface IEventPageParams {
    id: string
}

const EventPage: React.FC = () => {
    const bem = bemClasses('EventPage')
    const {id} = useParams<IEventPageParams>()
    const history = useHistory()
    const isAuth = useAppSelector(selectIsAuth)

    const fetchEventRequest = useCallback(() => fetchOneEventsRequest(id), [id])
    const [event, eventIsLoading, eventError] = useApi<IEvent>(fetchEventRequest)

    return (
        <div className={bem.block()}>
            <div className='container'>
                {eventIsLoading && (
                    <div className={bem.element('preloader')}>
                        <Spin/>
                    </div>
                )}
                {!eventIsLoading && event && (
                    <div>
                        <h3>{event.title}</h3>
                        <p>{event.description}</p>
                        <p>
                            c {format(new Date(event.startDate), 'dd.mm.yyyy')} по {format(new Date(event.finishDate), 'dd.mm.yyyy')}
                        </p>
                        {!isAuth && (
                            <p>Залогиньтесь что бы принять участие!</p>
                        )}
                        {isAuth && (
                            <Button
                                type={'primary'}
                                onClick={() => {
                                    /*todo найти или написать самому инструмент
                                    с помощью которого можно "собирать" url с параметрами
                                    что бы можно было использовать переменые роутов (EVENTS_ROUTE например)
                                    типа этого - const url = buildUrl(EVENT_REGISTER_ROUTE, {id: '42'})
                                    в результать 'events/42/register'
                                    */
                                    history.push(`/events/${id}/register`)
                                    // history.push(`login`)
                                }}>
                                Принять участие
                            </Button>
                        )}
                    </div>
                )}
            </div>
        </div>
    )
}

export default EventPage
