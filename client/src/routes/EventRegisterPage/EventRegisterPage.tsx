import React from 'react'
import bemClasses from '../../utils/bemClasses'
import './EventRegisterPage.scss'

const EventRegisterPage: React.FC = () => {
    const bem = bemClasses('EventRegisterPage')

    return (
        <div className={bem.block()}>
            EventRegisterPage
        </div>
    )
}

export default EventRegisterPage
