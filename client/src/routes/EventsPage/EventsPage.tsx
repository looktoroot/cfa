import React, {useCallback} from 'react'
import {Spin, Pagination} from 'antd'
import {useApi, useAppDispatch, useAppSelector} from '../../hooks'
import {fetchAllEventsRequest} from '../../http/eventsApi'
import bemClasses from '../../utils/bemClasses'
import EventItem from './views/EventItem'
import {
    selectEvents, selectEventsLimit,
    selectEventsPage,
    selectEventsTotalCount,
    setEvents,
    setEventsTotalCount,
    setEventsPage
} from '../../reducers/eventsReducer'
import {IEventsData} from '../../types/IEventsData'
import './EventsPage.scss'

const EventsPage: React.FC = () => {
    const bem = bemClasses('EventsPage')
    const dispatch = useAppDispatch()
    const events = useAppSelector(selectEvents)
    const eventsTotalCount = useAppSelector(selectEventsTotalCount)
    const eventsPage = useAppSelector(selectEventsPage)
    const eventsLimit = useAppSelector(selectEventsLimit)

    const handlePageChange = useCallback((page) => {
        dispatch(setEventsPage(page))
    }, [dispatch])

    const fetchEventsRequest = useCallback(() => fetchAllEventsRequest({
        page: eventsPage,
        limit: eventsLimit
    }), [eventsPage, eventsLimit])

    const fetchEventsCallback = useCallback((data: IEventsData) => {
        dispatch(setEvents(data.rows))
        dispatch(setEventsTotalCount(data.count))
    }, [dispatch])
    const [eventsData, eventsIsLoading, eventsError] = useApi<IEventsData>(fetchEventsRequest, fetchEventsCallback)

    return (
        <div className={bem.block()}>
            <div className='container'>
                {eventsIsLoading && (
                    <div className={bem.element('preloader')}>
                        <Spin/>
                    </div>
                )}
                {!eventsIsLoading && events && (
                    <>
                        <div className={bem.element('list')}>
                            {events.map(event => (
                                <EventItem key={event.id} {...event}/>
                            ))}
                        </div>
                        {eventsLimit < eventsTotalCount && (
                            <Pagination
                                current={eventsPage}
                                total={eventsTotalCount}
                                pageSize={eventsLimit}
                                onChange={handlePageChange}
                            />
                        )}
                    </>
                )}
            </div>
        </div>
    )
}

export default EventsPage
