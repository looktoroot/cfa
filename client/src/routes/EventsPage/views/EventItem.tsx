import React from 'react'
import {useHistory} from 'react-router-dom'
import {Button} from 'antd'
import {format} from 'date-fns'
import {IEvent} from '../../../types/IEvent'
import bemClasses from '../../../utils/bemClasses'
import './EventItem.scss'
import {EVENTS_ROUTE} from '../../index'

const EventItem: React.FC<IEvent> = ({id, title, description, startDate, finishDate}) => {
    const bem = bemClasses('EventItem')
    const history = useHistory()

    return (
        <div className={bem.block()}>
            <h3>{title}</h3>
            <p>{description}</p>
            <p>
                c {format(new Date(startDate), 'dd.mm.yyyy')} по {format(new Date(finishDate), 'dd.mm.yyyy')}
            </p>
            <Button
                type={'primary'}
                onClick={() => {
                history.push(`${EVENTS_ROUTE}/${id}`)
            }}>
                Подробнее
            </Button>
        </div>
    )
}

export default EventItem
