import React from 'react'
import {Link, useHistory} from 'react-router-dom'
import {Input, Button, notification} from 'antd'
import {useForm, Controller, SubmitHandler} from 'react-hook-form'
import {EyeInvisibleOutlined, EyeTwoTone} from '@ant-design/icons'
import FormField from '../../shared/FormField/FormField'
import {ILoginValues, login} from '../../http/authApi'
import bemClasses from '../../utils/bemClasses'
import {useAppDispatch} from '../../hooks'
import {setUser, setIsAuth} from '../../reducers/authReducer'
import {EVENTS_ROUTE, REGISTRATION_ROUTE} from '../index'
import './LoginPage.scss'


const LoginPage: React.FC = () => {
    const bem = bemClasses('LoginPage')
    const dispatch = useAppDispatch()
    const history = useHistory()

    const {control, handleSubmit, reset, formState: {errors, isSubmitting}} = useForm<ILoginValues>()
    const onSubmit: SubmitHandler<ILoginValues> = (values) => {
        login(values)
            .then(({id, email, role}) => {
                dispatch(setUser({id, email, role}))
                dispatch(setIsAuth(true))
                notification.success({message: 'Вы успешно вошли'})
                reset()
                history.push(EVENTS_ROUTE)
            })
            .catch(err => {
                const {message, errors} = err.response.data

                if (errors) {
                    //todo server validate
                } else {
                    notification.error({message})
                }
            })
    }

    return (
        <div className={bem.block()}>
            <form
                className={bem.element('form')}
                onSubmit={handleSubmit(onSubmit)}
            >
                <span className={bem.element('form-title')}>Вход</span>
                <Controller
                    name='email'
                    control={control}
                    rules={{required: 'Обязательное поле'}}
                    render={({field}) => (
                        <FormField
                            className={bem.element('form-field')}
                            label={'Email:'}
                            error={errors[field.name]}
                        >
                            <Input {...field}/>
                        </FormField>
                    )}
                />
                <Controller
                    name='password'
                    control={control}
                    rules={{required: 'Обязательное поле'}}
                    render={({field}) => (
                        <FormField
                            className={bem.element('form-field')}
                            label={'Пароль:'}
                            error={errors[field.name]}
                        >
                            <Input.Password
                                {...field}
                                iconRender={visible => (visible ? <EyeTwoTone/> : <EyeInvisibleOutlined/>)}
                            />
                        </FormField>
                    )}
                />
                <div className={bem.element('form-actions')}>
                    <span>
                        Нет аккаунта? <Link to={REGISTRATION_ROUTE}>Зарегистрируйтесь!</Link>
                    </span>
                    <Button
                        htmlType='submit'
                        type={'primary'}
                        loading={isSubmitting}
                        disabled={isSubmitting}
                    >
                        Войти
                    </Button>
                </div>
            </form>
        </div>
    )
}

export default LoginPage
