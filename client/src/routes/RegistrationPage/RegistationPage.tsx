import React from 'react'
import {Button, Input, notification} from 'antd'
import {useForm, Controller, SubmitHandler} from 'react-hook-form'
import {EyeInvisibleOutlined, EyeTwoTone} from '@ant-design/icons'
import FormField from '../../shared/FormField/FormField'
import {IRegistrationValues, registration} from '../../http/authApi'
import bemClasses from '../../utils/bemClasses'
import {Link, useHistory} from 'react-router-dom'
import {LOGIN_ROUTE} from '../index'
import './RegistrationPage.scss'

const RegistrationPage = () => {
    const bem = bemClasses('RegistrationPage')
    const history = useHistory()

    const {control, handleSubmit, reset, formState: {errors, isSubmitting}} = useForm<IRegistrationValues>()
    const onSubmit: SubmitHandler<IRegistrationValues> = (values) => {
        registration(values)
            .then(() => {
                notification.success({message: 'Вы успешно зарегистрировались'})
                reset()
                history.push(LOGIN_ROUTE)
            })
            .catch(err => {
                const {message, errors} = err.response.data

                if (errors) {
                    //todo server validate
                } else {
                    notification.error({message})
                }
            })
    }

    return (
        <div className={bem.block()}>
            <form
                className={bem.element('form')}
                onSubmit={handleSubmit(onSubmit)}
            >
                <span className={bem.element('form-title')}>Регистрация</span>
                <Controller
                    name='email'
                    control={control}
                    rules={{required: 'Обязательное поле'}}
                    render={({field}) => (
                        <FormField
                            className={bem.element('form-field')}
                            label={'Email:'}
                            error={errors[field.name]}
                        >
                            <Input {...field}/>
                        </FormField>
                    )}
                />
                <Controller
                    name='password'
                    control={control}
                    rules={{required: 'Обязательное поле'}}
                    render={({field}) => (
                        <FormField
                            className={bem.element('form-field')}
                            label={'Пароль:'}
                            error={errors[field.name]}
                        >
                            <Input.Password
                                {...field}
                                iconRender={visible => (visible ? <EyeTwoTone/> : <EyeInvisibleOutlined/>)}
                            />
                        </FormField>
                    )}
                />
                <div className={bem.element('form-actions')}>
                    <span>
                        Уже есть аккаунт? <Link to={LOGIN_ROUTE}>Войдите!</Link>
                    </span>
                    <Button
                        htmlType='submit'
                        type={'primary'}
                        loading={isSubmitting}
                        disabled={isSubmitting}
                    >
                        Зарегистрироваться
                    </Button>
                </div>
            </form>

        </div>
    )
}

export default RegistrationPage
