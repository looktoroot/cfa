import React from 'react'
import {Controller, SubmitHandler, useForm} from 'react-hook-form'
import {Button, Input, DatePicker, notification} from 'antd'
import bemClasses from '../../../utils/bemClasses'
import {IEvent} from '../../../types/IEvent'
import {createEvent, IEventValues} from '../../../http/eventsApi'
import FormField from '../../../shared/FormField'
import './CreateEventPage.scss'

const CreateEventPage: React.FC = () => {
    const bem = bemClasses('CreateEventPage')

    const {control, handleSubmit, reset, formState: {errors, isSubmitting}} = useForm<IEvent>()
    const onSubmit: SubmitHandler<IEventValues> = (values) => {
        createEvent(values)
            .then((data) => {
                notification.success({message: 'Эвент успешно создан'})
                //todo поля дат DatePicker не сбрасываются
                reset()
            })
            .catch(err => {
                const {message, errors} = err.response.data

                if (errors) {
                    //todo server validate
                } else {
                    notification.error({message})
                }
            })
    }

    return (
        <div className={bem.block()}>
            <div className={'container'}>
                <h2 className={bem.element('title')}>
                    Создание эвента
                </h2>

                <form
                    className={bem.element('form')}
                    onSubmit={handleSubmit(onSubmit)}
                >
                    <Controller
                        name='title'
                        control={control}
                        rules={{required: 'Обязательное поле'}}
                        render={({field}) => (
                            <FormField
                                className={bem.element('form-field')}
                                label={'Название:'}
                                error={errors[field.name]}
                            >
                                <Input {...field}/>
                            </FormField>
                        )}
                    />
                    <Controller
                        name='description'
                        control={control}
                        rules={{required: 'Обязательное поле'}}
                        render={({field}) => (
                            <FormField
                                className={bem.element('form-field')}
                                label={'Описание:'}
                                error={errors[field.name]}
                            >
                                <Input.TextArea {...field}/>
                            </FormField>
                        )}
                    />
                    <div className={bem.element('form-row')}>
                        <Controller
                            name='startDate'
                            control={control}
                            rules={{required: 'Обязательное поле'}}
                            render={(props) => (
                                <FormField
                                    className={bem.element('form-field')}
                                    label={'Начало:'}
                                    error={errors[props.field.name]}
                                >
                                    <DatePicker
                                        {...props}
                                        picker="date"
                                        onChange={(date, dateString) => {
                                            props.field.onChange(dateString)
                                        }}

                                    />
                                </FormField>
                            )}
                        />
                        <Controller
                            name='finishDate'
                            control={control}
                            rules={{required: 'Обязательное поле'}}
                            render={(props) => (
                                <FormField
                                    className={bem.element('form-field')}
                                    label={'Конец:'}
                                    error={errors[props.field.name]}
                                >
                                    <DatePicker
                                        {...props}
                                        picker="date"
                                        onChange={(date, dateString) => {
                                            props.field.onChange(dateString)
                                        }}

                                    />
                                </FormField>
                            )}
                        />
                    </div>
                    <div className={bem.element('form-actions')}>
                        <Button
                            htmlType='submit'
                            type={'primary'}
                            loading={isSubmitting}
                            disabled={isSubmitting}
                        >
                            Создать
                        </Button>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default CreateEventPage
