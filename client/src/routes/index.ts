import React from 'react'
import Roles from '../enums/Roles'
import AdminPage from './admin/AdminPage'
import CreateEventPage from './admin/CreateEventPage'
import LoginPage from './LoginPage'
import RegistrationPage from './RegistrationPage'
import EventPage from './EventPage'
import IndexPage from './IndexPage'
import EventsPage from './EventsPage'
import EventRegisterPage from './EventRegisterPage'

export const INDEX_ROUTE = '/'
export const LOGIN_ROUTE = '/login'
export const REGISTRATION_ROUTE = '/registration'
export const EVENTS_ROUTE = '/events'
export const EVENT_ROUTE = '/events/:id'
export const EVENT_REGISTER_ROUTE = '/events/:id/register'
export const ADMIN_ROUTE = '/admin'
export const CREATE_EVENT_ROUTE = '/admin/event'

export interface IRoute {
    path: string,
    roles: string[],
    component: React.FC,
}


const routes: IRoute[] = [
    {
        path: INDEX_ROUTE,
        roles: Roles.getKeys(),
        component: IndexPage,
    },
    {
        path: ADMIN_ROUTE,
        roles: [Roles.ADMIN],
        component: AdminPage,
    },
    {
        path: CREATE_EVENT_ROUTE,
        roles: [Roles.ADMIN],
        component: CreateEventPage,
    },
    {
        path: LOGIN_ROUTE,
        roles: Roles.getKeys(),
        component: LoginPage,
    },
    {
        path: REGISTRATION_ROUTE,
        roles: Roles.getKeys(),
        component: RegistrationPage,
    },
    {
        path: EVENT_ROUTE,
        roles: Roles.getKeys(),
        component: EventPage,
    },
    {
        path: EVENTS_ROUTE,
        roles: Roles.getKeys(),
        component: EventsPage,
    },
    {
        path: EVENT_REGISTER_ROUTE,
        roles: Roles.getAuth(),
        component: EventRegisterPage,
    },
]

export default routes
