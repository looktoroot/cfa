import React from 'react'
import {Switch, Redirect, Route} from 'react-router-dom'
import routes, {INDEX_ROUTE} from '../../routes'
import Roles from '../../enums/Roles'
import {useAppSelector} from '../../hooks'
import {selectUser} from '../../reducers/authReducer'

const AppRouter: React.FC = () => {

    const user = useAppSelector(selectUser)
    const role = user ? user.role : Roles.GUEST

    return (
        <Switch>
            {routes
                .filter(({roles}) => roles.includes(role))
                .map(({path, component}) => (
                    <Route key={path} path={path} component={component} exact/>
                ))
            }
            <Redirect to={INDEX_ROUTE}/>
        </Switch>
    )
}

export default AppRouter
