import React from 'react'
import {FieldError} from 'react-hook-form'
import bemClasses from '../../utils/bemClasses'
import './FormField.scss'

interface IFormFiled {
    className?: string,
    label?: string,
    error?: FieldError
}

const FormField: React.FC<IFormFiled> = ({children, error, label, className}) => {
    const bem = bemClasses('FormField')

    return (
        <div className={bem.join(bem.block(), className)}>
            {label && (
                <span className={bem.element('label')}>
                    {label}
                </span>
            )}
            {children}
            {error && error.message && (
                <div className={bem.element('error')}>
                    {error.message}
                </div>
            )}
        </div>
    )
}

export default FormField
