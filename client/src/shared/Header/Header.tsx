import React from 'react'
import {Link, useHistory} from 'react-router-dom'
import {Button} from 'antd'
import Roles from '../../enums/Roles'
import bemClasses from '../../utils/bemClasses'
import routes, {CREATE_EVENT_ROUTE, LOGIN_ROUTE, REGISTRATION_ROUTE} from '../../routes'
import {useAppDispatch, useAppSelector} from '../../hooks'
import {logout} from '../../http/authApi'
import {selectIsAuth, selectUser, setIsAuth, setUser} from '../../reducers/authReducer'
import './Header.scss'

const Header: React.FC = () => {
    const bem = bemClasses('Header')
    const dispatch = useAppDispatch()
    const user = useAppSelector(selectUser)
    const isAuth = useAppSelector(selectIsAuth)
    const history = useHistory()

    const logoutHandler = () => {
        logout()
        dispatch(setUser(null))
        dispatch(setIsAuth(false))
    }

    return (
        <div className={bem.block()}>
            <div className={bem.element('logo')}>
                CFA
            </div>
            <div className={bem.element('nav')}>
                {routes.map(({path}) => (
                    <Link key={path} to={path}>
                        {path} &nbsp; &nbsp;
                    </Link>
                ))}
            </div>
            <div className={bem.element('controls')}>
                {isAuth && (
                    <>
                        {user?.role === Roles.ADMIN && (
                            <Button onClick={() => {
                                history.push(CREATE_EVENT_ROUTE)
                            }}>
                                Создать эвент
                            </Button>
                        )}
                        <Button onClick={logoutHandler}>
                            Выйти
                        </Button>
                        <div>
                            User: {user?.email || ' - '}
                        </div>
                    </>
                )}
                {!isAuth && (
                    <>
                        <Button onClick={() => {
                            history.push(LOGIN_ROUTE)
                        }}>
                            Войти
                        </Button>
                        <Button onClick={() => {
                            history.push(REGISTRATION_ROUTE)
                        }}>
                            Зарегистрироваться
                        </Button>
                    </>
                )}
            </div>
        </div>
    )
}

export default Header
