import React, {useCallback} from 'react'
import {Spin} from 'antd'
import bemClasses from '../../utils/bemClasses'
import {useApi, useAppDispatch} from '../../hooks'
import {IUser} from '../../types/IUser'
import {userInitRequest} from '../../http/authApi'
import {setIsAuth, setUser} from '../../reducers/authReducer'
import Header from '../Header/Header'
import './Layout.scss'

const Layout: React.FC = ({children}) => {
    const bem = bemClasses('Layout')
    const dispatch = useAppDispatch()

    const userInitCallback = useCallback(({id, email, role}) => {
        dispatch(setUser({id, email, role}))
        dispatch(setIsAuth(true))
    }, [dispatch])

    const [contextUser, contextUserIsLoading] = useApi<IUser>(userInitRequest, userInitCallback)

    return (
        <div className={bem.block()}>
            <Header/>
            <div className={bem.element('content')}>
                {contextUserIsLoading && (
                    <div className={bem.element('preloader')}>
                        <Spin/>
                    </div>
                )}
                {!contextUserIsLoading && children}
            </div>
        </div>
    )
}

export default Layout
