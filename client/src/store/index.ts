import {configureStore} from '@reduxjs/toolkit'
import count from '../reducers/countReducer'
import auth from '../reducers/authReducer'
import events from '../reducers/eventsReducer'

const store = configureStore({
    reducer: {
        count,
        auth,
        events
    },
})


export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch

export default store
