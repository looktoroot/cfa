export interface IEvent {
    id: number
    title: string
    description: string
    startDate: string
    finishDate: string
}
