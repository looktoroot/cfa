import {IEvent} from './IEvent'

export interface IEventsData {
    count: number
    rows: IEvent[]
}
