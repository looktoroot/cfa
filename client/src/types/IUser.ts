import Roles from '../enums/Roles'
const roles = Roles.getKeys()

export interface IUser {
    id: number
    email: string
    role: typeof roles[number]
}
