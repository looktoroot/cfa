
type IBemModifier = string | {[key: string]: boolean | string | number}

const bemClasses = (blockName: string) => {
    return {
        block: (modifiers?: IBemModifier) => {
            return applyModifiers(blockName, modifiers)
        },
        element: (elementName: string, modifiers?: IBemModifier) => {
            return applyModifiers(blockName + '__' + elementName, modifiers)
        },
        join: (...names: (string | undefined)[]) => {
            return names.filter(Boolean).join(' ')
        }
    }
}

const applyModifiers = (entity: string, modifiers?: IBemModifier) => {
    let result = []
    result.push(entity)

    if (typeof modifiers === 'string') {
        result.push(entity + (modifiers ? '_' + modifiers : ''))
    } else if (modifiers) {
        Object.keys(modifiers).forEach((key) => {
            const value = modifiers[key]
            if (!value) {
                // Skip
            }
            else if (value === true) {
                result.push(entity + '_' + key)
            }
            else {
                result.push(entity + '_' + key + '_' + value)
            }
        })
    }

    return result.join(' ')
}

export default bemClasses
