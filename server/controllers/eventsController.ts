import {Request, Response} from 'express'
import {Event} from '../models/models'

interface IEventsQuery {
    limit: number
    page: number
}

class EventsController {

    async create(req: Request, res: Response) {
        const {title, description, startDate, finishDate} = req.body
        const event = await Event.create({title, description, startDate, finishDate})
        return res.json(event)
    }

    async getAll(req: Request<any, any, any, IEventsQuery>, res: Response) {
        let {limit = 50, page = 1} = req.query
        let offset = limit * page - limit

        const events = await Event.findAndCountAll({limit, offset})
        return res.json(events)
    }

    async getOne(req: Request, res: Response) {
        const {eventId} = req.params
        const event = await Event.findOne({where: {id: eventId}})
        return res.json(event)
    }
}

export default new EventsController()
