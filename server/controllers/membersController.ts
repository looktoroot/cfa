import {NextFunction, Request, Response} from 'express'
import {Member} from '../models/models'
import ApiError from '../error/ApiError'

class MembersController {

    async create(req: Request, res: Response, next: NextFunction) {
        const {eventId} = req.params
        const {id: userId} = req.user

        const candidate = await Member.findOne({where: {eventId, userId}})
        if (candidate) {
            return next(ApiError.badRequest('Вы уже участник этих соревнований'))
        }

        const member = await Member.create({eventId, userId})
        return res.json(member)
    }

    async getAll(req: Request, res: Response) {
        const {eventId} = req.params

        const members = await Member.findAll({where: {eventId}})
        return res.json(members)
    }

    async getOne(req: Request, res: Response) {
        const {memberId} = req.params
        const track = await Member.findOne({where: {id: memberId}})
        return res.json(track)
    }
}

export default new MembersController()
