import {NextFunction, Request, Response} from 'express'
import {Member, Passage, User} from '../models/models'
import ApiError from '../error/ApiError'

class PassagesController {

    async create(req: Request, res: Response, next: NextFunction) {
        const {top, bonus, total} = req.body
        const {eventId, trackId} = req.params
        const {id: userId} = req.user

        const member = await Member.findOne({where: {eventId, userId}})
        if (!member) {
            return next(ApiError.badRequest('Вы не являйтель участником совернования'))
        }

        const memberId = member.getDataValue('id')
        const candidate = await Passage.findOne({where: {trackId, memberId}})
        if (candidate) {
            return next(ApiError.badRequest('Прохождение трассы уже создано'))
        }

        const passage = await Passage.create({top, bonus, total, trackId, memberId})
        return res.json(passage)
    }

    async update(req: Request, res: Response, next: NextFunction) {
        const {top, bonus, total} = req.body
        const {passageId} = req.params
        await Passage.update({top, bonus, total}, {where: {id: passageId}})
        const passage = await Passage.findOne({where: {id: passageId}})
        return res.json(passage)
    }

    async getAll(req: Request, res: Response) {
        const {trackId} = req.params
        const passages = await Passage.findAll({where: {trackId}})
        return res.json(passages)
    }

    async getOne(req: Request, res: Response) {
        const {passageId} = req.params
        const passage = await Passage.findOne({where: {id: passageId}})
        return res.json(passage)
    }
}

export default new PassagesController()
