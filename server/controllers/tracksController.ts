import {Request, Response} from 'express'
import {Track} from '../models/models';

class TracksController {

    async create(req: Request, res: Response) {
        const {title, color, sector} = req.body
        const {eventId} = req.params
        const track = await Track.create({title, color, sector, eventId})
        return res.json(track)
    }

    async getAll(req: Request, res: Response) {
        const {eventId} = req.params
        const tracks = await Track.findAll({where: {eventId}})
        return res.json(tracks)
    }

    async getOne(req: Request, res: Response) {
        const {eventId, trackId} = req.params
        const track = await Track.findOne({where: {id: trackId, eventId}})
        return res.json(track)
    }
}

export default new TracksController()
