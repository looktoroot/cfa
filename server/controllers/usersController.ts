import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
import {Request, Response, NextFunction} from 'express'
import ApiError from '../error/ApiError'
import {User} from '../models/models'

const generateJwt = (id: number, email: string, role: string) => {
    return jwt.sign(
        {id, email, role},
        process.env.SECRET_KEY!,
        {expiresIn: '24h'}
    )
}

class UsersController {

    async registration(req: Request, res: Response, next: NextFunction) {
        const {email, password, role} = req.body

        if (!email || !password) {
            return next(ApiError.badRequest('Некорректный email или пароль'))
        }

        const candidate = await User.findOne({where: {email}})
        if (candidate) {
            return next(ApiError.badRequest('Пользователь с таким email уже существует'))
        }

        const hashPassword = await bcrypt.hash(password, 5)
        const user = await User.create({email, role, password: hashPassword})

        return res.json({id: user.getDataValue('id')})
    }

    async login(req: Request, res: Response, next: NextFunction) {
        const {email, password} = req.body

        const user = await User.findOne({where: {email}})
        if (!user) {
            return next(ApiError.badRequest('Пользователь не найден'))
        }

        const comparePassword = bcrypt.compareSync(password, user.getDataValue('password'))
        if (!comparePassword) {
            return next(ApiError.badRequest('Не верный пароль'))
        }

        const token = generateJwt(
            user.getDataValue('id'),
            user.getDataValue('email'),
            user.getDataValue('role')
        )

        return res.json({token})
    }

    async init(req: Request, res: Response, next: NextFunction) {
        const user = await User.findOne({where: {email: req.user.email}})

        if (!user) {
            return next(ApiError.badRequest('Пользователь не найден'))
        }

        return res.json({
            id: user.getDataValue('id'),
            email: user.getDataValue('email'),
            role: user.getDataValue('role'),
        })
    }

}

export default new UsersController()
