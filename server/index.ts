import dotenv from 'dotenv'
dotenv.config()
import express from 'express'
import cors from 'cors'
import sequelize from './db'
import './models/models'
import router from './routes'
import errorMiddleware from './middlewares/errorHandlingMiddleware'


const PORT = process.env.PORT || 5000
const app = express()
app.use(cors())
app.use(express.json())
app.use('/api/v1/', router)

//last middleware
app.use(errorMiddleware)

const start = async () => {
    try {
        await sequelize.authenticate()
        await sequelize.sync()
        // await sequelize.drop()
        app.listen(PORT, () => {
            console.log(`⚡️[server]: Server is running at http://localhost:${PORT}`)
        })
    } catch (e) {
        console.log(e)
    }
}

start()
