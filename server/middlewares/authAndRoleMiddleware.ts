import {Response, Request, NextFunction} from 'express'
import jwt from 'jsonwebtoken'
import {IUserToken} from '../types/IUserToken'
import ApiError from '../error/ApiError'

export default (checkedRole?: string) => (req: Request, res: Response, next: NextFunction) => {
    if (req.method === 'OPTIONS') {
        next()
    }

    try {
        const token = req.headers.authorization?.split(' ')[1]
        if (!token) {
            return next(ApiError.unauthorized('Не авторизован'))
        } else {
            const decodedUser = jwt.verify(token, process.env.SECRET_KEY!)
            if (checkedRole && (decodedUser as IUserToken).role !== checkedRole) {
                return next(ApiError.forbiden('Нет доступа'))
            }
            req.user = decodedUser as IUserToken
            next()
        }
    } catch (e) {
        return next(ApiError.unauthorized('Не авторизован'))
    }
}
