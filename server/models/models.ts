import sequelize from '../db'
import {DataTypes} from 'sequelize'

export const User = sequelize.define('user', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    email: {type: DataTypes.STRING, unique: true},
    password: {type: DataTypes.STRING},
    name: {type: DataTypes.STRING},
    role: {type: DataTypes.STRING, defaultValue: 'user'},
})

export const Event = sequelize.define('event', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    title: {type: DataTypes.STRING, allowNull: false},
    description: {type: DataTypes.STRING, allowNull: false},
    startDate: {type: DataTypes.DATE, allowNull: false},
    finishDate: {type: DataTypes.DATE, allowNull: false},
})

export const Track = sequelize.define('track', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    title: {type: DataTypes.STRING, allowNull: false},
    color: {type: DataTypes.STRING, allowNull: false},
    sector: {type: DataTypes.INTEGER, allowNull: false},
})

export const Passage = sequelize.define('passage', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    top: {type: DataTypes.INTEGER},
    bonus: {type: DataTypes.INTEGER},
    total: {type: DataTypes.INTEGER, allowNull: false},
})

export const Member = sequelize.define('member', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
})

Event.hasMany(Track)
Track.belongsTo(Event)

Track.hasMany(Passage)
Passage.belongsTo(Track)

User.belongsToMany(Event, {through: Member})
Event.belongsToMany(User, {through: Member})

Member.hasMany(Passage)
Passage.belongsTo(Member)



