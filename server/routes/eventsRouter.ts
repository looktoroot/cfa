import express from 'express'
import eventsController from '../controllers/eventsController'
import authAndRoleMiddleware from '../middlewares/authAndRoleMiddleware'

const router = express.Router()

//TODO: add roles enum
router.post('/', authAndRoleMiddleware('admin'), eventsController.create)
router.get('/', eventsController.getAll)
router.get('/:eventId', eventsController.getOne)


export default router
