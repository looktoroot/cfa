import express from 'express'
import usersRouter from './usersRouter'
import eventsRouter from './eventsRouter'
import membersRouter from './membersRouter'
import tracksRouter from './tracksRouter'
import passagesRouter from './passagesRouter'

const router = express.Router()

router.use('/users', usersRouter)
router.use('/events', eventsRouter)
router.use('/events/:eventId/members', membersRouter)
router.use('/events/:eventId/tracks', tracksRouter)
router.use('/events/:eventId/tracks/:trackId/passages', passagesRouter)

export default router
