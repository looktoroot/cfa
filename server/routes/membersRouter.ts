import express from 'express'
import membersController from '../controllers/membersController'
import authAndRoleMiddleware from '../middlewares/authAndRoleMiddleware'

const router = express.Router({mergeParams: true})

router.post('/', authAndRoleMiddleware(), membersController.create)
router.get('/', membersController.getAll)
router.get('/:memberId', membersController.getOne)

export default router
