import express from 'express'
import passagesController from '../controllers/passagesController'
import authAndRoleMiddleware from '../middlewares/authAndRoleMiddleware'

const router = express.Router({mergeParams: true})

router.post('/', authAndRoleMiddleware(), passagesController.create)
router.get('/', passagesController.getAll)
router.get('/:passageId', passagesController.getOne)
router.post('/:passageId', passagesController.update)

export default router
