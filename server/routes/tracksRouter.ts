import express from 'express'
import tracksController from '../controllers/tracksController'
import authAndRoleMiddleware from '../middlewares/authAndRoleMiddleware'

const router = express.Router({mergeParams: true})

router.post('/', authAndRoleMiddleware('admin'), tracksController.create)
router.get('/', tracksController.getAll)
router.get('/:trackId', tracksController.getOne)

export default router
