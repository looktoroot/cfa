import express from 'express'
import usersController from '../controllers/usersController'
import authAndRoleMiddleware from '../middlewares/authAndRoleMiddleware'

const router = express.Router()

router.post('/registration', usersController.registration)
router.post('/login', usersController.login)
router.get('/init', authAndRoleMiddleware(), usersController.init)

export default router
